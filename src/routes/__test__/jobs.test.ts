import faker from 'faker';
import request from 'supertest';
import { createJob, createUser, findJobById } from '../../test/db';
import { app } from '../../test/setup';
import { signIn } from '../../test/signin-mock';
import { CreateJobDTO } from '../jobs';

const generateJobInput = (input?: Partial<CreateJobDTO>) => {
  return {
    title: faker.name.jobTitle(),
    type: faker.name.jobType(),
    applicationURL: faker.internet.url(),
    remote: true,
    companyName: faker.random.word(),
    companyWebsite: faker.internet.url(),
    ...input,
  };
};

describe('Jobs router', () => {
  describe('Create a job', () => {
    const createJobPath = '/api/jobs';

    it('should require authentication', async () => {
      await request(app).post(createJobPath).send().expect(401);
    });

    it('should ensure required fields are provided', async () => {
      const user = await createUser();
      const res = await request(app)
        .post(createJobPath)
        .set('Cookie', await signIn(user.id, app))
        .send()
        .expect(400);
      expect(res.body).toHaveProperty('errors');
      const invalidFields = res.body.errors.map(({ field }: any) => field);
      expect(invalidFields).toEqual(
        expect.arrayContaining([
          'title',
          'type',
          'applicationURL',
          'remote',
          'companyName',
          'companyWebsite',
        ])
      );
    });

    it('should create a job if all required properties are provided', async () => {
      const requestBody = generateJobInput();
      const user = await createUser();
      const res = await request(app)
        .post(createJobPath)
        .set('Cookie', await signIn(user.id, app))
        .send(requestBody)
        .expect(201);
      expect(res.body.id).toBeDefined();
      expect(res.body).toMatchObject({
        ...requestBody,
        user: {
          id: user.id,
        },
      });
      expect(res.body).not.toHaveProperty('description');
      expect(res.body).not.toHaveProperty('locationCity');
      expect(res.body).not.toHaveProperty('locationCountry');
      expect(res.body).not.toHaveProperty('remoteLocation');
      expect(res.body).not.toHaveProperty('remoteLocation');
    });

    it('should create a job if all properties are provided', async () => {
      const requestBody = generateJobInput({
        description: faker.random.words(3),
        locationCity: faker.address.city(),
        locationCountry: faker.address.country(),
        remoteLocation: faker.address.country(),
      });
      const user = await createUser();
      const res = await request(app)
        .post(createJobPath)
        .set('Cookie', await signIn(user.id, app))
        .send(requestBody)
        .expect(201);

      expect(res.body).toMatchObject(requestBody);
    });
  });

  describe('Delete a job', () => {
    it('should delete a job', async () => {
      const user = await createUser();
      const cookie = await signIn(user.id, app);
      const createJobRes = await request(app)
        .post('/api/jobs')
        .set('Cookie', cookie)
        .send(generateJobInput())
        .expect(201);

      await request(app)
        .delete(`/api/jobs/${createJobRes.body.id}`)
        .set('Cookie', cookie)
        .send()
        .expect(204);

      const deletedJob = await findJobById(createJobRes.body.id);

      expect(deletedJob?.deletedAt).toBeDefined();
    });
  });

  describe('Get jobs', () => {
    it('should return a list of jobs', async () => {
      const user = await createUser();
      const job1 = await createJob({ ...generateJobInput(), user });
      const job2 = await createJob({ ...generateJobInput(), user });
      const res = await request(app)
        .get('/api/jobs')
        .query({ limit: 2 })
        .send()
        .expect(200);
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body.map((job: any) => job.id)).toEqual(
        expect.arrayContaining([job1.id, job2.id])
      );
    });
  });

  describe('Put job', () => {
    it('should update a job', async () => {
      const user = await createUser();
      const job = await createJob({ ...generateJobInput(), user });
      const updatePayload = {
        title: `${job.title} new`,
        description: `${job.description} new`,
        type: `${job.type} new`,
        applicationURL: `${job.applicationURL}/new`,
        locationCity: `${job.locationCity} new`,
        locationCountry: `${job.locationCountry} new`,
        remote: !job.remote,
        remoteLocation: `${job.remoteLocation} new`,
        companyName: `${job.companyName} new`,
        companyWebsite: `${job.companyWebsite}/new`,
      };
      const res = await request(app)
        .put(`/api/jobs/${job.id}`)
        .set('Cookie', await signIn(user.id, app))
        .send(updatePayload)
        .expect(200);
      expect(res.body).toMatchObject({ id: job.id, ...updatePayload });
    });
  });
});
