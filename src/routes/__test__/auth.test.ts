import passport from 'passport';
import request from 'supertest';
import { createApp } from '../../app';
import { createUser } from '../../test/db';
import { app } from '../../test/setup';
import { signIn } from '../../test/signin-mock';

describe('Auth routes', () => {
  describe('/auth/linkedin', () => {
    it('should redirect to LinkedIn for authentication', () => {
      return request(app).get('/api/auth/linkedin').send().expect(302);
    });
  });

  describe('/auth/linkedin/callback', () => {
    const passportAuthMock = jest.fn((_req, _res, next) => next());
    const tmpAuthenticate = passport.authenticate;
    passport.authenticate = () => passportAuthMock;

    afterAll(() => {
      passport.authenticate = tmpAuthenticate;
    });

    let app2 = createApp();

    it('should render the html that sends a success message to the opener window', async () => {
      const response = await request(app2)
        .get('/api/auth/linkedin/callback')
        .send()
        .expect(200);

      expect(passportAuthMock).toHaveBeenCalled();
      expect(response.text).toContain(
        `window.opener.postMessage('SIGNIN_SUCCEEDED', '${process.env.FRONTEND_ORIGIN}')`
      );
    });
  });

  describe('/auth/current-user', () => {
    it('returns null if the user is not signed in', () => {
      return request(app).get('/api/auth/current-user').send().expect('null');
    });

    it("returns the user's data if the user is signed in", async () => {
      const user = await createUser({
        email: 'john@example.com',
        name: 'John',
      });

      const response = await request(app)
        .get('/api/auth/current-user')
        .set('Cookie', await signIn(user.id, app))
        .send()
        .expect(200);

      expect(response.body).toBeTruthy();
      expect(response.body.email).toBe(user.email);
    });
  });

  describe('/auth/signout', () => {
    it('should sign the current user out', async () => {
      const user = await createUser({
        email: 'john2@example.com',
        name: 'John',
      });
      const cookie = await signIn(user.id, app);

      const curUserResponse = await request(app)
        .get('/api/auth/current-user')
        .set('Cookie', cookie)
        .send()
        .expect(200);

      expect(curUserResponse.body).toBeTruthy();
      expect(curUserResponse.body.email).toBe(user.email);

      await request(app)
        .post('/api/auth/signout')
        .set('Cookie', cookie)
        .send()
        .expect(204);

      await request(app)
        .get('/api/auth/current-user')
        .set('Cookie', cookie)
        .send()
        .expect(200, 'null');
    });
  });
});
