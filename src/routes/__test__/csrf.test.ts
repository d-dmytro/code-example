import request from 'supertest';
import { app } from '../../test/setup';

describe('CSRF route', () => {
  it('should respond with a token', async () => {
    const res = await request(app).get('/api/csrf').send().expect(200);
    expect(res.body.token).toBeDefined();
  });
});
