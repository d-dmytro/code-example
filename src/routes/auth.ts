import { Router } from 'express';
import passport from 'passport';

const createAuthRouter = () => {
  const authRouter = Router();

  /**
   * @openapi
   *   /auth/linkedin:
   *     get:
   *       summary: Authenticate a user using the LinkedIn OAuth2 strategy.
   *       responses:
   *         302:
   *           description: Redirect the user to LinkedIn for authentication.
   */
  authRouter.get('/api/auth/linkedin', passport.authenticate('linkedin'));

  /**
   * @openapi
   *   /auth/linkedin/callback:
   *     get:
   *       summary: Handle OAuth2 redirect from LinkedIn.
   *       responses:
   *         200:
   *           description: Render an HTML page that notifies the parent window about successful authentication.
   *           content:
   *             text/html:
   *               type: string
   */
  authRouter.get(
    '/api/auth/linkedin/callback',
    passport.authenticate('linkedin'),
    (_req, res) => {
      res.render('auth/linkedin-callback', {
        origin: process.env.FRONTEND_ORIGIN,
      });
    }
  );

  /**
   * @openapi
   *   /auth/current-user:
   *     get:
   *       summary: Get the current user.
   *       responses:
   *         200:
   *           description: Successful operation.
   *           content:
   *             application/json:
   *               schema:
   *                 $ref: '#/components/schemas/User'
   */
  authRouter.get('/api/auth/current-user', (req, res) => {
    res.json(req.user ? req.user.serialize() : null);
  });

  /**
   * @openapi
   *   /auth/signout:
   *     post:
   *       summary: Sign the user out.
   *       responses:
   *         204:
   *           description: Successful operation.
   */
  authRouter.post('/api/auth/signout', (req, res) => {
    req.logout();
    res.sendStatus(204);
  });

  return authRouter;
};

export default createAuthRouter;
