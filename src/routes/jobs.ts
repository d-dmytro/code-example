import express from 'express';
import { body, param, query } from 'express-validator';
import { JobDTO } from '../entities/job';
import requireAuth from '../middlewares/require-auth';
import validateRequest from '../middlewares/validate-request';
import JobsService from '../services/jobs';

/**
 * @openapi
 *   components:
 *     schemas:
 *       JobInputDTO:
 *         type: object
 *         properties:
 *           title:
 *             type: string
 *           description:
 *             type: string
 *           type:
 *             type: string
 *           applicationURL:
 *             type: string
 *           locationCity:
 *             type: string
 *           locationCountry:
 *             type: string
 *           remote:
 *             type: boolean
 *           remoteLocation:
 *             type: string
 *           companyName:
 *             type: string
 *           companyWebsite:
 *             type: string
 *         required:
 *           - title
 *           - type
 *           - applicationURL
 *           - remote
 *           - companyName
 *           - companyWebsite
 */
interface JobInputDTO {
  title: string;
  description?: string;
  type: string;
  applicationURL: string;
  locationCity?: string;
  locationCountry?: string;
  remote: boolean;
  remoteLocation?: string;
  companyName: string;
  companyWebsite: string;
}

/**
 * @openapi
 *   components:
 *     schemas:
 *       CreateJobDTO:
 *         $ref: '#/components/schemas/JobInputDTO'
 */
export interface CreateJobDTO extends JobInputDTO {}

/**
 * @openapi
 *   components:
 *     schemas:
 *       PutJobDTO:
 *         $ref: '#/components/schemas/JobInputDTO'
 */
export interface PutJobDTO extends JobInputDTO {}

const createJobsRouter = ({ jobsService }: { jobsService: JobsService }) => {
  const router = express.Router();

  const checkJobInput = [
    body('title').notEmpty().bail().isString(),
    body('description').optional().isString(),
    body('type').notEmpty().bail().isString(),
    body('applicationURL').notEmpty().bail().isURL(),
    body('locationCity').optional().notEmpty().bail().isString(),
    body('locationCountry').optional().notEmpty().bail().isString(),
    body('remote').isBoolean(),
    body('remoteLocation').optional().notEmpty().bail().isString(),
    body('companyName').notEmpty().bail().isString(),
    body('companyWebsite').notEmpty().bail().isString(),
  ];

  /**
   * @openapi
   *   /jobs:
   *     post:
   *       summary: Create a job.
   *       requestBody:
   *         required: true
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/CreateJobDTO'
   *       responses:
   *         201:
   *           description: Job was created.
   *           content:
   *             application/json:
   *               schema:
   *                 $ref: '#/components/schemas/Job'
   *         400:
   *           $ref: '#/components/responses/InvalidInput'
   *         401:
   *           $ref: '#/components/responses/NotAuthorized'
   */
  router.post<any, any, CreateJobDTO>(
    '/api/jobs',
    requireAuth,
    ...checkJobInput,
    validateRequest,
    async (req, res) => {
      const job = await jobsService.createJob({
        input: req.body,
        user: req.user!,
      });

      res.status(201).json(job.serialize());
    }
  );

  interface DeleteParams {
    id: number;
    [key: string]: any;
  }

  /**
   * @openapi
   *   /jobs/{id}:
   *     delete:
   *       summary: Delete a job.
   *       parameters:
   *         - in: path
   *           name: id
   *           schema:
   *             type: integer
   *           required: true
   *           description: The id of the task to delete.
   *       responses:
   *         204:
   *           description: Deleted successfully.
   *         400:
   *           $ref: '#/components/responses/InvalidInput'
   *         403:
   *           $ref: '#/components/responses/Forbidden'
   *         404:
   *           $ref: '#/components/responses/NotFound'
   */
  router.delete<DeleteParams>(
    '/api/jobs/:id',
    requireAuth,
    param('id').isInt({ min: 1 }).toInt(),
    validateRequest,
    async (req, res) => {
      const { id } = req.params;

      await jobsService.deleteJob({ jobId: id, user: req.user! });

      res.sendStatus(204);
    }
  );

  interface GetJobsQuery {
    limit: number;
    [key: string]: any;
  }

  /**
   * @openapi
   *   /jobs:
   *     get:
   *       summary: Find jobs.
   *       parameters:
   *         - in: query
   *           name: limit
   *           schema:
   *             type: number
   *           required: true
   *           description: The number of items to return at max.
   *       responses:
   *         200:
   *           description: A list of jobs.
   *           content:
   *             application/json:
   *               schema:
   *                 type: array
   *                 items:
   *                   $ref: '#/components/schemas/Job'
   */
  router.get<any, JobDTO[], any, GetJobsQuery>(
    '/api/jobs',
    query('limit').notEmpty().bail().isInt({ min: 1 }),
    validateRequest,
    async (req, res) => {
      const jobs = await jobsService.findJobs({ limit: req.query.limit });
      res.json(jobs.map((job) => job.serialize()));
    }
  );

  interface UpdateJobParams {
    id: number;
    [key: string]: any;
  }

  /**
   * @openapi
   *   /jobs/{id}:
   *     put:
   *       summary: Update a job.
   *       parameters:
   *         - in: path
   *           name: id
   *           schema:
   *             type: integer
   *           required: true
   *           description: The id of the job to update.
   *       requestBody:
   *         required: true
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/UpdateJobDTO'
   *       responses:
   *         200:
   *           description: Job was updated.
   *           content:
   *             application/json:
   *               schema:
   *                 $ref: '#/components/schemas/Job'
   *         400:
   *           $ref: '#/components/responses/InvalidInput'
   *         401:
   *           $ref: '#/components/responses/NotAuthorized'
   *         403:
   *           $ref: '#/components/responses/Forbidden'
   *         404:
   *           $ref: '#/components/responses/NotFound'
   */
  router.put<UpdateJobParams, JobDTO, PutJobDTO>(
    '/api/jobs/:id',
    requireAuth,
    param('id').isInt({ min: 1 }).toInt(),
    ...checkJobInput,
    validateRequest,
    async (req, res) => {
      const { id } = req.params;
      const job = await jobsService.putJob({
        jobId: id,
        input: req.body,
        user: req.user!,
      });
      res.json(job.serialize());
    }
  );

  return router;
};

export default createJobsRouter;
