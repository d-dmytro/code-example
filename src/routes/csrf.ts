import { Router } from 'express';
import csrfProtection from '../middlewares/csrf';

const createCSRFRouter = () => {
  const router = Router();

  /**
   * @openapi
   *   /csrf:
   *     get:
   *       responses:
   *         200:
   *           description: Returns a CSRF protection token.
   *           content:
   *             application/json:
   *               schema:
   *                 type: object
   *                 properties:
   *                   token: string
   */
  router.get('/api/csrf', csrfProtection, (req, res) => {
    res.json({ token: req.csrfToken() });
  });

  return router;
};

export default createCSRFRouter;
