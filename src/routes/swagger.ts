import express from 'express';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';

/**
 * @openapi
 *   components:
 *     responses:
 *       InvalidInput:
 *         description: Invalid input.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Errors'
 *       NotAuthorized:
 *         description: Not authorized.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Errors'
 *       Forbidden:
 *         description: Forbidden.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Errors'
 *       NotFound:
 *         description: Not found.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Errors'
 *     schemas:
 *       Error:
 *         type: object
 *         properties:
 *           field:
 *             type: string
 *           message:
 *             type: string
 *         required:
 *           - message
 *       Errors:
 *         type: object
 *         properties:
 *           errors:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/Error'
 *
 */

const options: swaggerJSDoc.Options = {
  definition: {
    openapi: '3.0.1',
    info: { title: 'Code Example API', version: '0.0.1' },
    servers: [{ name: 'Local server', url: 'http://localhost:3000/api' }],
    components: {
      securitySchemes: {
        cookieAuth: {
          type: 'apiKey',
          in: 'cookie',
          name: 'sessionid',
        },
      },
    },
  },
  apis: ['./src/routes/*.ts', './src/entities/*.ts'],
};

const swaggerRouter = express.Router();

const swaggerDocument = swaggerJSDoc(options);
swaggerRouter.use(
  '/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocument)
);

export default swaggerRouter;
