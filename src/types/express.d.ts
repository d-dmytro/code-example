import { User as UserEntity } from '../entities/user';

declare global {
  namespace Express {
    interface User extends UserEntity {}
  }
}
