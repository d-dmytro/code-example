import 'passport-linkedin-oauth2';

declare module 'passport-linkedin-oauth2' {
  interface StrategyOption {
    scope?: string[];
    state?: boolean;
  }
}
