namespace NodeJS {
  interface ProcessEnv {
    POSTGRES_HOST: string;
    POSTGRES_USER: string;
    POSTGRES_PASSWORD: string;
    POSTGRES_DB: string;
    SESSION_SECRET: string;
    SESSION_COOKIE_NAME: string;
    SESSION_COOKIE_DOMAIN: string;
    LINKEDIN_KEY: string;
    LINKEDIN_SECRET: string;
    FRONTEND_ORIGIN: string;
    BASE_URL: string;
  }
}
