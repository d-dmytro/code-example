import _ from 'lodash';
import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { SerializableEntity } from './serializable-entity';
import { User, UserDTO } from './user';

/**
 * @openapi
 *   components:
 *     schemas:
 *       Job:
 *         type: object
 *         properties:
 *           id:
 *             type: integer
 *           title:
 *             type: string
 *           description:
 *             type: string
 *           type:
 *             type: string
 *           applicationURL:
 *             type: string
 *             format: url
 *           locationCity:
 *             type: string
 *           locationCountry:
 *             type: string
 *           remote:
 *             type: boolean
 *           remoteLocation:
 *             type: string
 *           companyName:
 *             type: string
 *           companyWebsite:
 *             type: string
 *           user:
 *             oneOf:
 *               - $ref: '#/components/schemas/User'
 *               - type: string
 *                 format: uuid
 *         required:
 *           - id
 *           - title
 *           - type
 *           - applicationURL
 *           - remote
 *           - companyName
 *           - companyWebsite
 */
export interface JobDTO {
  id: number;
  title: string;
  description?: string;
  type: string;
  applicationURL: string;
  locationCity?: string;
  locationCountry?: string;
  remote: boolean;
  remoteLocation?: string;
  companyName: string;
  companyWebsite: string;
  user: UserDTO | string;
}

@Entity('jobs')
export class Job implements SerializableEntity<JobDTO> {
  @PrimaryGeneratedColumn('increment')
  id!: number;

  @Column({ type: 'varchar' })
  title!: string;

  @Column({ type: 'varchar', nullable: true })
  description?: string;

  @Column({ type: 'varchar', name: 'job_type' })
  type!: string;

  @Column({ type: 'varchar', name: 'application_url' })
  applicationURL!: string;

  @Column({ type: 'varchar', name: 'location_city', nullable: true })
  locationCity?: string;

  @Column({ type: 'varchar', name: 'location_country', nullable: true })
  locationCountry?: string;

  @Column({ type: 'boolean' })
  remote!: boolean;

  @Column({ type: 'varchar', name: 'remote_location', nullable: true })
  remoteLocation?: string;

  @Column({ type: 'varchar', name: 'company_name' })
  companyName!: string;

  @Column({ type: 'varchar', name: 'company_website' })
  companyWebsite!: string;

  @Column({ type: 'uuid', name: 'user_id' })
  userId!: string;

  @ManyToOne(() => User, (user) => user.jobs)
  @JoinColumn({ name: 'user_id' })
  user?: User;

  @DeleteDateColumn({ name: 'deleted_at' })
  deletedAt?: Date;

  serialize() {
    const serialized = {
      id: this.id,
      title: this.title,
      description: this.description,
      type: this.type,
      applicationURL: this.applicationURL,
      locationCity: this.locationCity,
      locationCountry: this.locationCountry,
      remote: this.remote,
      remoteLocation: this.remoteLocation,
      companyName: this.companyName,
      companyWebsite: this.companyWebsite,
      user: this.user ? this.user.serialize() : this.userId,
    };

    return _.omitBy(serialized, _.isNil) as any;
  }
}
