export interface SerializableEntity<T extends object> {
  serialize(): T;
}
