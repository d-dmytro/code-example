import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { tableNames } from '../lib/constants';
import { Job } from './job';
import { SerializableEntity } from './serializable-entity';

/**
 * @openapi
 *  components:
 *    schemas:
 *      User:
 *        type: object
 *        properties:
 *          id:
 *            type: string
 *            format: uuid
 *          email:
 *            type: string
 *            format: email
 *          name:
 *            type: string
 */
export interface UserDTO {
  id: string;
  email: string;
  name: string;
}

@Entity(tableNames.users)
export class User implements SerializableEntity<UserDTO> {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column({
    type: 'varchar',
    unique: true,
    name: 'linkedin_id',
    nullable: true,
  })
  linkedinId: string | null = null;

  @Column({ type: 'varchar', unique: true })
  email!: string;

  @Column({ type: 'varchar' })
  name!: string;

  @OneToMany(() => Job, (job) => job.user)
  jobs?: Job[];

  serialize() {
    return {
      id: this.id,
      email: this.email,
      name: this.name,
    };
  }
}
