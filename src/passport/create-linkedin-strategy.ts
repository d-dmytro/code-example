import { Strategy } from 'passport-linkedin-oauth2';
import { getRepository } from 'typeorm';
import { User } from '../entities/user';

const createLinkedinStrategy = () => {
  return new Strategy(
    {
      clientID: process.env.LINKEDIN_KEY,
      clientSecret: process.env.LINKEDIN_SECRET,
      callbackURL: `${process.env.BASE_URL}/api/auth/linkedin/callback`,
      scope: ['r_emailaddress', 'r_liteprofile'],
      state: true,
    },
    async (_accessToken, _refreshToken, profile, done) => {
      const userRepo = getRepository(User);

      try {
        const user = await userRepo.findOne({
          linkedinId: profile.id,
        });

        if (user) {
          done(null, user);
        } else {
          const newUser = userRepo.create({
            linkedinId: profile.id,
            email: profile.emails[0].value,
            name:
              profile.displayName ||
              `${profile.name.givenName} ${profile.name.familyName}`,
          });

          await userRepo.save(newUser);

          done(null, newUser);
        }
      } catch (e) {
        done(e);
      }
    }
  );
};

export default createLinkedinStrategy;
