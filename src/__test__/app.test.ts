import request from 'supertest';
import { createApp } from '../app';

const app = createApp();

describe('Basic app behavior tests', () => {
  it('returns 404 if a route is not found', async () => {
    const response = await request(app)
      .get('/non-existent-route')
      .send()
      .expect(404);
    expect(response.body).toMatchObject({
      errors: [{ message: 'Not found' }],
    });
  });
});
