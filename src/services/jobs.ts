import _ from 'lodash';
import { getRepository } from 'typeorm';
import { Job } from '../entities/job';
import { User } from '../entities/user';
import { ForbiddenError } from '../errors/forbidden-error';
import { NotFoundError } from '../errors/not-found-error';
import { CreateJobDTO, PutJobDTO } from '../routes/jobs';

interface CreateJobOptions {
  user: User;
  input: CreateJobDTO;
}

interface DeleteJobOptions {
  user: User;
  jobId: number;
}

interface FindJobsOptions {
  limit: number;
}

interface PutJobOptions {
  jobId: number;
  input: PutJobDTO;
  user: User;
}

class JobsService {
  private allowedJobInputProps = [
    'title',
    'description',
    'type',
    'applicationURL',
    'locationCity',
    'locationCountry',
    'remote',
    'remoteLocation',
    'companyName',
    'companyWebsite',
  ];

  async createJob(options: CreateJobOptions) {
    const jobRepo = getRepository(Job);

    const job = jobRepo.create({
      ..._.pick(options.input, this.allowedJobInputProps),
      user: options.user,
    });

    return await jobRepo.save(job);
  }

  async deleteJob({ user, jobId }: DeleteJobOptions) {
    const jobRepo = getRepository(Job);

    const job = await jobRepo.findOne(jobId);

    if (!job) {
      throw new NotFoundError();
    }

    if (job.userId !== user.id) {
      throw new ForbiddenError();
    }

    const result = await jobRepo.softDelete(job.id);

    if (!_.isNil(result.affected) && result.affected < 1) {
      throw new Error(`Could not delete job ${job.id}`);
    }

    return job;
  }

  async findJobs({ limit }: FindJobsOptions) {
    const jobRepo = getRepository(Job);
    const jobs = await jobRepo.find({ order: { id: -1 }, take: limit });
    return jobs;
  }

  async putJob({ jobId, input, user }: PutJobOptions) {
    const jobRepo = getRepository(Job);
    const job = await jobRepo.findOne(jobId);

    if (!job) {
      throw new NotFoundError();
    }

    if (job.userId !== user.id) {
      throw new ForbiddenError();
    }

    return jobRepo.save(
      jobRepo.merge(job, _.pick(input, this.allowedJobInputProps))
    );
  }
}

export default JobsService;
