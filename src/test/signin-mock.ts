import { Application } from 'express';
import request from 'supertest';

export const addSignInHandlerMock = (app: Application) => {
  app.get('/signin-mock', (req, res) => {
    (req.session as any).passport = { user: req.query.id };
    res.end();
  });
};

export const signIn = async (userId: string, app: Application) => {
  const response = await request(app)
    .get('/signin-mock')
    .query({ id: userId })
    .send()
    .expect(200);

  return response.get('Set-Cookie');
};
