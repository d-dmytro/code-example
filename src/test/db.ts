import faker from 'faker';
import { createConnection, DeepPartial, getRepository } from 'typeorm';
import { Job } from '../entities/job';
import { User } from '../entities/user';

export const connect = ({ database }: { database?: string } = {}) => {
  return createConnection({
    type: 'postgres',
    host: process.env.POSTGRES_HOST,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database,
    entities: ['src/entities/*.ts'],
    migrations: ['db/migrations/*.ts'],
  });
};

export const createDatabase = async (database: string) => {
  const conn = await connect();
  await conn.query(`DROP DATABASE IF EXISTS ${database}`);
  await conn.query(`CREATE DATABASE ${database}`);
  await conn.close();
};

export const deleteDatabase = async (database: string) => {
  const conn = await connect();
  await conn.query(`DROP DATABASE IF EXISTS ${database}`);
  await conn.close();
};

interface CreateUserData {
  email: string;
  name: string;
}

export const createUser = async (data?: CreateUserData) => {
  const userRepo = getRepository(User);
  const user = userRepo.create(
    data || { email: faker.internet.email(), name: faker.name.firstName() }
  );
  await userRepo.save(user);
  return user;
};

export const createJob = async (data: DeepPartial<Job>) => {
  const jobRepo = getRepository(Job);
  const job = jobRepo.create(data);
  await jobRepo.save(job);
  return job;
};

export const findJobById = (jobId: number) => {
  const jobRepo = getRepository(Job);
  return jobRepo.findOne(jobId, { withDeleted: true });
};
