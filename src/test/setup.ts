import dotenv from 'dotenv';
import { Connection } from 'typeorm';
import { createApp } from '../app';
import { connect, createDatabase, deleteDatabase } from './db';
import { addSignInHandlerMock } from './signin-mock';

dotenv.config({ path: '.env.test' });

export const app = createApp({ addHandlers: addSignInHandlerMock });

let connection: Connection;

// Create a database for each jest worker.
const database = `${process.env.POSTGRES_DB}_${process.env.JEST_WORKER_ID}`;

beforeAll(async () => {
  await createDatabase(database);
  connection = await connect({ database });
  await connection.runMigrations();
});

afterAll(async () => {
  if (connection) {
    await connection.close();
    await deleteDatabase(database);
  }
});
