import { CustomError } from './custom-error';

export class ForbiddenError extends CustomError {
  statusCode = 403;

  constructor() {
    super('Forbidden');
  }

  serializeErrors() {
    return [{ message: this.message }];
  }
}
