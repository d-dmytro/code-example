import connectRedis from 'connect-redis';
import cors from 'cors';
import express, { Application } from 'express';
import 'express-async-errors';
import session from 'express-session';
import helmet from 'helmet';
import passport from 'passport';
import pinoHTTP from 'pino-http';
import redis from 'redis';
import 'reflect-metadata';
import { getRepository } from 'typeorm';
import { User } from './entities/user';
import { NotFoundError } from './errors/not-found-error';
import { isProd, isTest } from './lib/constants';
import errorHandler from './middlewares/error-handler';
import createLinkedinStrategy from './passport/create-linkedin-strategy';
import createAuthRouter from './routes/auth';
import createCSRFRouter from './routes/csrf';
import createJobsRouter from './routes/jobs';
import swaggerRouter from './routes/swagger';
import JobsService from './services/jobs';

interface Hooks {
  addHandlers?: (app: Application) => void;
}

const createApp = ({ addHandlers }: Hooks = {}) => {
  const app = express();

  app.set('view engine', 'ejs');

  app.use(helmet({ contentSecurityPolicy: false }));

  app.use(
    pinoHTTP({
      autoLogging: false,
      prettyPrint: !isProd,
      level: isTest ? 'warn' : 'info',
    })
  );

  app.use(express.json());

  const RedisStore = connectRedis(session);

  app.use(
    session({
      secret: process.env.SESSION_SECRET,
      name: process.env.SESSION_COOKIE_NAME,
      resave: false, // Don't save the session if it hasn't been modified
      saveUninitialized: false, // Don't save the session if it's new and not modified
      cookie: {
        signed: false, // Disable encryption
        secure: isProd, // In production add cookies to HTTPS requests only
        httpOnly: true,
        domain: process.env.SESSION_COOKIE_DOMAIN,
      },
      store: isTest
        ? undefined
        : new RedisStore({ client: redis.createClient() }),
    })
  );

  passport.use(createLinkedinStrategy());
  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser<string>((id, done) => {
    getRepository(User)
      .findOne({ id })
      .then((user) => done(null, user))
      .catch(done);
  });

  app.use(cors({ origin: process.env.FRONTEND_ORIGIN, credentials: true }));

  const jobsService = new JobsService();

  app.use(createCSRFRouter());
  app.use(createAuthRouter());
  app.use(createJobsRouter({ jobsService }));

  if (addHandlers) {
    addHandlers(app);
  }

  app.use(swaggerRouter);

  app.all('*', () => {
    throw new NotFoundError();
  });

  app.use(errorHandler);

  return app;
};

export { createApp };
