import rateLimit from 'express-rate-limit';
import RedisStore from 'rate-limit-redis';
import { isTest } from '../lib/constants';

interface RateLimiterArgs {
  key: 'ip' | 'userId';
  max: number; // Max number of connections
  duration: number; // milliseconds
  keyPrefix?: string;
}

const tooManyRequestsError = {
  errors: [{ message: 'Too many requests, please try again later.' }],
};

const createRateLimiter = ({
  key = 'ip',
  max,
  duration,
  keyPrefix = 'rl:',
}: RateLimiterArgs) => {
  return rateLimit({
    store: isTest ? undefined : new RedisStore({ prefix: keyPrefix }),
    max,
    windowMs: duration,
    message: tooManyRequestsError as any,
    keyGenerator: (req) =>
      key === 'userId' && req.user ? req.user.id : req.ip,
  });
};

export default createRateLimiter;
