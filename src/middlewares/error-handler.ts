import { NextFunction, Request, Response } from 'express';
import { isObjectLike } from 'lodash';
import { CustomError } from '../errors/custom-error';

interface HTTPError {
  statusCode: number;
  message: string;
}

const isHTTPError = (err: any): err is HTTPError =>
  isObjectLike(err) &&
  typeof err.statusCode === 'number' &&
  typeof err.message === 'string';

type ErrorResponse = Response<{
  errors: { message: string; field?: string }[];
}>;

const logByStatusCode = (err: HTTPError, req: Request) => {
  if (err.statusCode < 500) {
    req.log.info(err);
  } else {
    req.log.error(err);
  }
};

const errorHandler = (
  err: Error,
  req: Request,
  res: ErrorResponse,
  _next: NextFunction
) => {
  if (err instanceof CustomError) {
    logByStatusCode(err, req);
    res.status(err.statusCode).json({ errors: err.serializeErrors() });
  } else if (isHTTPError(err)) {
    logByStatusCode(err, req);
    res.status(err.statusCode).json({ errors: [{ message: err.message }] });
  } else {
    req.log.error(err);
    res.status(500).json({ errors: [{ message: 'Internal server error' }] });
  }
};

export default errorHandler;
