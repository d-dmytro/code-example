import { RequestHandler } from 'express';
import { NotAuthorizedError } from '../errors/not-authorized-error';

const requireAuth: RequestHandler = (req, _res, next) => {
  if (!req.isAuthenticated()) {
    throw new NotAuthorizedError();
  }

  next();
};

export default requireAuth;
