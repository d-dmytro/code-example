import { createConnection } from 'typeorm';
import { createApp } from './app';

const port = process.env.PORT || 3000;

const start = async () => {
  await createConnection();

  const app = createApp();

  app.listen(port, () => {
    console.log(`Listening on port ${port}`);
  });
};

start().catch(console.error);
