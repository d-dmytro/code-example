# Code Example

## Before installation and tests

You need to run a MySQL server. The project includes docker-compose.yml for the MySQL setup for dev and test environments.
Please check docker-compose.yml and modify if necessary.

To run Docker containers defined in docker-compose in background:

```
docker-compose up -d
```

To delete containers launched by docker-compose:

```
docker-compose down
```

## Installation

```
npm i
```

## Development

1. Copy ".env.example" to ".env" and fill it in.
2. Run `npm run dev`

## Tests

Tests use connection credentials from .env.test. Before all tests the databases with names `${process.env.POSTGRES_DB}_${process.env.JEST_WORKER_ID}` are dropped and created.

1. Copy ".env.example" to ".env.test" and fill it in.
2. Run `npm test`
