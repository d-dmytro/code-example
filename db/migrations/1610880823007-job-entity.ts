import { MigrationInterface, QueryRunner } from 'typeorm';

export class jobEntity1610880823007 implements MigrationInterface {
  name = 'jobEntity1610880823007';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "jobs" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying, "job_type" character varying NOT NULL, "application_url" character varying NOT NULL, "location_city" character varying, "location_country" character varying, "remote" boolean NOT NULL, "remote_location" character varying, "company_name" character varying NOT NULL, "company_website" character varying NOT NULL, "user_id" uuid NOT NULL, CONSTRAINT "PK_cf0a6c42b72fcc7f7c237def345" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "jobs" ADD CONSTRAINT "FK_9027c8f0ba75fbc1ac46647d043" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "jobs" DROP CONSTRAINT "FK_9027c8f0ba75fbc1ac46647d043"`
    );
    await queryRunner.query(`DROP TABLE "jobs"`);
  }
}
