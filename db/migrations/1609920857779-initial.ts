import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1609920857779 implements MigrationInterface {
    name = 'initial1609920857779'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "linkedin_id" character varying, "email" character varying NOT NULL, "name" character varying NOT NULL, CONSTRAINT "UQ_19d6c798f6ec73e07b8582676a7" UNIQUE ("linkedin_id"), CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "users"`);
    }

}
