import {MigrationInterface, QueryRunner} from "typeorm";

export class jobDelete1610908362328 implements MigrationInterface {
    name = 'jobDelete1610908362328'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "jobs" ADD "deleted_at" TIMESTAMP`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "jobs" DROP COLUMN "deleted_at"`);
    }

}
